package main

import (
    "fmt"
    "github.com/nlopes/slack"
)

// Bot is the bot
type Bot struct {
    client  *slack.Client
    rtm     *slack.RTM
    channel string
}

// NewBot creates the bot for specified channel.
func NewBot(c *slack.Client, r *slack.RTM, channel string) *Bot {
    b := &Bot{c, r, channel}

    go r.ManageConnection()

    return b
}

func (b *Bot) handleMessage(ev *slack.MessageEvent) error {
    fmt.Printf("Message: %v\n", ev)
    if ev.Text == "Hello" {
        user, err := b.client.GetUserInfo(ev.Msg.User)
        if err != nil {
            fmt.Printf("%s\n", err)
            return err
        }
        helloUser := fmt.Sprintf("Hello %s", user.Name)
        b.rtm.SendMessage(b.rtm.NewOutgoingMessage(helloUser, b.channel))
    }
    return nil
}

func (b *Bot) run() {
    for {
        select {
        case msg := <-b.rtm.IncomingEvents:
            fmt.Print("Event received: ")
            switch ev := msg.Data.(type) {
            case *slack.HelloEvent:
                // ignore hello
            case *slack.ConnectedEvent:
                fmt.Println("Infos:", ev.Info)
                fmt.Println("Connection counter:", ev.ConnectionCount)
            case *slack.MessageEvent:
                fmt.Printf("Message: %v\n", ev)
                b.handleMessage(ev)
            case *slack.PresenceChangeEvent:
                fmt.Printf("Presence Change: %v\n", ev)

            case *slack.LatencyReport:
                fmt.Printf("Current latency: %v\n", ev.Value)

            case *slack.RTMError:
                fmt.Printf("Error: %s\n", ev.Error())

            case *slack.InvalidAuthEvent:
                fmt.Printf("Invalid credentials")
                return

            default:
                // Ignore other events..
                // fmt.Printf("Unexpected: %v\n", msg.Data)
            }
        }
    }
}

func main() {
    api := slack.New("xoxb-11312913330-uhvaALni3zu4jRA7fjKzEtve")
    api.SetDebug(true)

    bot := NewBot(api, api.NewRTM(), "C07GDGG4C")
    bot.run()
}
